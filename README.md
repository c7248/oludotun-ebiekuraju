## Drones

[[_TOC_]]

---

:scroll: **START**

### About

This project provide a set of services for organizing and controlling a fleet
of drones used in medication delivery services. It is a service oriented RESTfull
application, hence did not provide any UI interface. All communications are done
through the API.

> Please note that there is a lot of room for improvement.

### Prerequisites

Before trying to build/run/test this application, ensure you have the following
installed on your machine:

- JDK 11+ (Java Development Kit)
- Set the JAVA_HOME environment variable pointing to your JDK installation

### Features

- Built with Spring Boot MVC
- In Memory DB with H2
- Using JPA to talk to the database
- Preloaded with test data

### Build

Open a terminal window and navigate to the project root folder where pom.xml 
is present and run the command below

```bash
./mvnw clean install
```

### Test

Open a terminal window and navigate to the project root folder where pom.xml
is present and run the command below:

```bash 
./mvnw test
```

### Run

Open a terminal window and navigate to the project root folder where pom.xml
is present and run the command below:

```bash
./mvnw spring-boot:run
```

### API
The API documentation is hosted on Postman Documenter [[View API Documentation](https://documenter.getpostman.com/view/9780106/UVsJvmNh)]

---

:scroll: **END**

