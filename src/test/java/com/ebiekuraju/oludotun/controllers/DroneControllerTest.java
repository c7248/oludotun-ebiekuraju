package com.ebiekuraju.oludotun.controllers;

import com.ebiekuraju.oludotun.beans.Drone;
import com.ebiekuraju.oludotun.repository.DroneRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(DroneController.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DroneControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private DroneRepository droneRepository;

    Drone DRONE_1 = new Drone("MST-97979000GF",
            Drone.Models.Heavyweight,
            400, 85, Drone.States.IDLE);
    Drone DRONE_2 = new Drone("MST-97979001GF",
            Drone.Models.Middleweight,
            300, 75, Drone.States.LOADED);
    Drone DRONE_3 = new Drone("MST-97979002GF",
            Drone.Models.Lightweight,
            200, 95, Drone.States.DELIVERING);

    @Test
    public void getAllDrones_success() throws Exception {
        List<Drone> drones = new ArrayList<>(Arrays.asList(DRONE_1, DRONE_2, DRONE_3));
        Mockito.when(droneRepository.findAll()).thenReturn(drones);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/drones")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[2].serialNumber", is("MST-97979002GF")));
    }

    @Test
    public void getAvailableDronesForLoading_success() throws Exception {
        List<Drone> drones = new ArrayList<>(Arrays.asList(DRONE_1, DRONE_2, DRONE_3));
        Mockito.when(droneRepository.availableForLoading()).thenReturn(drones);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/drones/ready")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].serialNumber", is("MST-97979000GF")));
    }

    @Test
    public void getDroneById_success() throws Exception {
        Optional<Drone> drone = Optional.of(DRONE_2);
        Mockito.when(droneRepository.findById("MST-97979001GF")).thenReturn(drone);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/drones/MST-97979001GF")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.serialNumber", is("MST-97979001GF")));
    }

    @Test
    public void getDroneBatteryLevel_success() throws Exception {
        Optional<Drone> drone = Optional.of(DRONE_2);
        Mockito.when(droneRepository.findById("MST-97979001GF")).thenReturn(drone);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/drones/MST-97979001GF/battery")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.batteryLevel", is(75)));
    }
}
