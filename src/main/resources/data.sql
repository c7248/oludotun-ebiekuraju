DELETE FROM drone;
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state)
VALUES ('MS-267881727LW', 'Lightweight', 150, 89, 'IDLE'),
    ('MSD-782636477MW', 'Middleweight', 250, 80, 'LOADING'),
    ('MSD-324598553CW', 'Cruiserweight', 400, 91, 'LOADED'),
    ('MSD-547828547HW', 'Heavyweight', 500, 77, 'DELIVERING'),
    ('MS-267881728LW', 'Lightweight', 150, 89, 'DELIVERED'),
    ('MSD-782636478MW', 'Middleweight', 250, 80, 'RETURNING'),
    ('MSD-324598554CW', 'Cruiserweight', 400, 21, 'IDLE'),
    ('MSD-547828548HW', 'Heavyweight', 500, 90, 'LOADING'),
    ('MSD-324598555CW', 'Cruiserweight', 400, 93, 'LOADED'),
    ('MSD-547828549HW', 'Heavyweight', 500, 60, 'DELIVERING');

DELETE FROM medication;
INSERT INTO medication (
    id,
    name,
    weight,
    code,
    image
) VALUES (1, 'Medication_1', 10, 'MED_CODE_01', '/med_image.jpg'),
    (2, 'Medication_2', 22, 'MED_CODE_02', '/med_image.jpg'),
    (3, 'Medication_3', 15, 'MED_CODE_03', '/med_image.jpg'),
    (4, 'Medication_4', 50, 'MED_CODE_04', '/med_image.jpg'),
    (5, 'Medication_5', 150, 'MED_CODE_05', '/med_image.jpg'),
    (6, 'Medication_6', 55, 'MED_CODE_06', '/med_image.jpg'),
    (7, 'Medication_7', 46, 'MED_CODE_07', '/med_image.jpg'),
    (8, 'Medication_8', 47, 'MED_CODE_08', '/med_image.jpg'),
    (9, 'Medication_9', 32, 'MED_CODE_09', '/med_image.jpg'),
    (10, 'Medication_10', 12, 'MED_CODE_10', '/med_image.jpg');

DELETE FROM dispatch;
INSERT INTO dispatch (
    id,
    drone_serial_number,
    medication_id,
    tracking_code,
    delivered,
    loaded_at
) VALUES (1, 'MS-267881727LW', 1, 'WB-00012100', false, CURRENT_TIMESTAMP),
    (2, 'MS-267881727LW', 2, 'WB-00012100', false, CURRENT_TIMESTAMP),
    (3, 'MS-267881727LW', 3, 'WB-00012100', false, CURRENT_TIMESTAMP);