package com.ebiekuraju.oludotun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class OludotunApplication {

	public static void main(String[] args) {
		SpringApplication.run(OludotunApplication.class, args);
	}

}
