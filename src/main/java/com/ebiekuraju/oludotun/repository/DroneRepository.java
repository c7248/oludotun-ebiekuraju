package com.ebiekuraju.oludotun.repository;

import com.ebiekuraju.oludotun.beans.Drone;
import com.ebiekuraju.oludotun.beans.Medication;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DroneRepository extends CrudRepository<Drone, String> {

    @Query("SELECT d FROM Drone d WHERE (d.state = 'IDLE' or d.state = 'LOADING') and d.batteryCapacity >= 25")
    public List<Drone> availableForLoading();

    @Query("SELECT md FROM Dispatch dp " +
            "JOIN dp.medication md " +
            "JOIN dp.drone dr " +
            "WHERE dr.serialNumber = ?1 and dp.delivered = false")
    public List<Medication> loadedMedications(String serialNumber);
}
