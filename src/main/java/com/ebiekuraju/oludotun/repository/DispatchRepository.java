package com.ebiekuraju.oludotun.repository;

import com.ebiekuraju.oludotun.beans.Dispatch;
import org.springframework.data.repository.CrudRepository;

public interface DispatchRepository extends CrudRepository<Dispatch, Integer> {

}
