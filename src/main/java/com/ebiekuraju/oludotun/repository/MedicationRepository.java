package com.ebiekuraju.oludotun.repository;

import com.ebiekuraju.oludotun.beans.Medication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends CrudRepository<Medication, Integer> {

}
