package com.ebiekuraju.oludotun.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ErrorServices {

    public static Map<String, Object> buildBody(String message) {
        Map<String, Object> body = new HashMap<>();
        List<Object> list = new ArrayList<>();
        Map<String, Object> error = new HashMap<>();
        error.put("defaultMessage", message);
        list.add(error);
        Iterable<Object> iterable = list;
        body.put("error", Boolean.TRUE);
        body.put("errors", iterable);
        return body;
    }
}
