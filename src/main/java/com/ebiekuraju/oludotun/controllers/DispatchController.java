package com.ebiekuraju.oludotun.controllers;

import com.ebiekuraju.oludotun.beans.Dispatch;
import com.ebiekuraju.oludotun.repository.DispatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dispatches")
public class DispatchController {

    @Autowired
    private DispatchRepository dispatchRepository;

    @GetMapping
    public Iterable<Dispatch> dispatches() {
        return dispatchRepository.findAll();
    }
}
