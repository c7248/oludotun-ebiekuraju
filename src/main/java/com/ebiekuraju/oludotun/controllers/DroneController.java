package com.ebiekuraju.oludotun.controllers;

import com.ebiekuraju.oludotun.beans.Dispatch;
import com.ebiekuraju.oludotun.beans.DispatchWrapper;
import com.ebiekuraju.oludotun.beans.Drone;
import com.ebiekuraju.oludotun.beans.Medication;
import com.ebiekuraju.oludotun.repository.DispatchRepository;
import com.ebiekuraju.oludotun.repository.DroneRepository;
import com.ebiekuraju.oludotun.repository.MedicationRepository;
import com.ebiekuraju.oludotun.services.ErrorServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/drones")
public class DroneController {
    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private DispatchRepository dispatchRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    @GetMapping
    public Iterable<Drone> drones() {
        return droneRepository.findAll();
    }

    @GetMapping("/ready")
    public Iterable<Drone> availableDrones() {
        return droneRepository.availableForLoading();
    }

    @GetMapping("/{serialNumber}")
    public Optional<Drone> drone(@PathVariable String serialNumber) {
        return droneRepository.findById(serialNumber);
    }

    @GetMapping("/{serialNumber}/medications")
    public Iterable<Medication> loadedMedications(@PathVariable String serialNumber) {
        return droneRepository.loadedMedications(serialNumber);
    }

    @GetMapping("/{serialNumber}/battery")
    public Map<String, Object> batteryLevel(@PathVariable String serialNumber) {
        Optional<Drone> drone = droneRepository.findById(serialNumber);
        Map<String, Object> result = new HashMap<>();
        result.put("serialNumber", drone.orElseThrow().getSerialNumber());
        result.put("batteryLevel", drone.orElseThrow().getBatteryCapacity());
        return result;
    }

    @PostMapping
    public ResponseEntity<Object> registerDrone(@Valid @RequestBody Drone drone,
                                                BindingResult result) {
        if(result.hasErrors()) {
            Map<String, Object> error = new HashMap<>();
            error.put("error", true);
            error.put("errors", result.getAllErrors());
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(droneRepository.save(drone), HttpStatus.OK);
    }

    @PutMapping("/{serialNumber}")
    public ResponseEntity<Object> updateDrone(@PathVariable String serialNumber,
                                              @Valid @RequestBody Drone drone,
                                              BindingResult result) {
        if(result.hasErrors()) {
            Map<String, Object> error = new HashMap<>();
            error.put("error", true);
            error.put("errors", result.getAllErrors());
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
        return (!droneRepository.existsById(serialNumber))?
                new ResponseEntity<>(droneRepository.save(drone),
                        HttpStatus.CREATED)
                : new ResponseEntity<>(droneRepository.save(drone),
                HttpStatus.OK);
    }

    @DeleteMapping("/{serialNumber}")
    public void deregisterDrone(@PathVariable String serialNumber) {
        droneRepository.deleteById(serialNumber);
    }

    @PostMapping("/{serialNumber}/medications")
    public ResponseEntity<Object> loadMedications(@PathVariable String serialNumber,
                                                  @RequestBody DispatchWrapper dispatchWrapper) {

        Drone drone = droneRepository.findById(serialNumber).get();
        //Prevent drone from loading if battery is less than 25% and state not IDLE of LOADING
        if(!drone.canLoad()) {
            Map<String, Object> error = ErrorServices
                    .buildBody("You can only load a drones in LOADING or IDLE state with battery not less than 25%");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
        // Change drone state to LOADING if it was IDLE
        if(drone.getState() == Drone.States.IDLE) {
            drone.setState(Drone.States.LOADING);
            droneRepository.save(drone);
        }
        // Check if drone is being overloaded
        List<Medication> loadedMedications = droneRepository.loadedMedications(drone.getSerialNumber());
        int loadedWeight = loadedMedications.stream().mapToInt(m -> m.getWeight()).sum();
        int newWeight = 0;
        Iterator<Medication> medications =
                medicationRepository.findAllById(dispatchWrapper.getMedicationIds()).iterator();
        List<Dispatch> dispatches = new ArrayList<>();
        while (medications.hasNext()) {
            Dispatch dispatch = new Dispatch();
            Medication medication = medications.next();
            dispatch.setDrone(drone);
            dispatch.setMedication(medication);
            dispatch.setDelivered(false);
            dispatch.setTrackingCode(dispatchWrapper.getTrackingCode());
            dispatch.setLoadedAt(LocalDateTime.now());
            dispatches.add(dispatch);
            newWeight += medication.getWeight();
        }
        if((newWeight + loadedWeight) > drone.getWeightLimit()) {
            Map<String, Object> error = ErrorServices
                    .buildBody("Drone maximum weight (" + drone.getWeightLimit() + "g) exceeded. "
                    + "You supplied " + newWeight + "g, and " + loadedWeight + "g is already loaded");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dispatchRepository.saveAll(dispatches), HttpStatus.OK);
    }
}
