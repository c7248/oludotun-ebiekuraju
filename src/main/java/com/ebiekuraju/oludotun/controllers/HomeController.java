package com.ebiekuraju.oludotun.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HomeController {

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> index() {
        Map<String, String> map = new HashMap<>();
        map.put("message", "Welcome to our novel drone delivery service.");
        return map;
    }
}
