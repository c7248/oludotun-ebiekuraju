package com.ebiekuraju.oludotun.controllers;

import com.ebiekuraju.oludotun.beans.Medication;
import com.ebiekuraju.oludotun.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/medications")
public class MedicationController {

    @Autowired
    private MedicationRepository medicationRepository;

    @GetMapping
    public Iterable<Medication> medications() {
        return medicationRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Medication> medication(@PathVariable int id) {
        return medicationRepository.findById(id);
    }

    @PostMapping
    public ResponseEntity<Object> addMedication(@Valid @RequestBody Medication medication, BindingResult result) {
        if(result.hasErrors()) {
            Map<String, Object> error = new HashMap<>();
            error.put("error", true);
            error.put("errors", result.getAllErrors());
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(medicationRepository.save(medication), HttpStatus.OK);
    }
}

