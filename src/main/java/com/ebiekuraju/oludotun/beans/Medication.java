package com.ebiekuraju.oludotun.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Set;


@Entity
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;

    @NotEmpty(message = "Name cannot be empty")
    @Pattern(regexp = "(^[a-zA-Z0-9_-]*$)", message = "Name can contain only letters, numbers, underscore and hyphen")
    private String name;

    @Max(value = Drone.MAX_WEIGHT_LIMIT, message = "Weight limit should not be more than 500")
    @Min(value = 1, message = "Weight limit should not be less than 1")
    private int weight;

    @Pattern(regexp = "(^[A-Z0-9_]*$)", message = "Code can contain only upper case letters, underscore and numbers")
    private String code;

    @NotEmpty(message = "Image path cannot be empty")
    private String image;

    @OneToMany(mappedBy = "medication")
    @JsonIgnore
    Set<Dispatch> dispatches;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Set<Dispatch> getDispatches() {
        return  dispatches;
    }
}
