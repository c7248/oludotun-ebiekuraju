package com.ebiekuraju.oludotun.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class Drone {
    public static enum Models {
        Lightweight,
        Middleweight,
        Cruiserweight,
        Heavyweight
    }
    public static enum States {
        IDLE,
        LOADING,
        LOADED,
        DELIVERING,
        DELIVERED,
        RETURNING
    }
    public static final int MAX_WEIGHT_LIMIT = 500;

    public Drone() {}
    public  Drone(String serialNumber,
                  Models model,
                  int weightLimit,
                  int batteryCapacity,
                  States state) {
        this.serialNumber = serialNumber;
        this.model = model;
        this.weightLimit = weightLimit;
        this.batteryCapacity = batteryCapacity;
        this.state = state;
    }

    @Id
    @Size(min = 3, max = 100, message = "Serial number must be between 3 and 100 characters")
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "model", length = 50)
    private Models model;

    @Max(value = MAX_WEIGHT_LIMIT, message = "Weight limit should not be more than 500")
    @Min(value = 1, message = "Weight limit should not be less than 1")
    private int weightLimit;

    @Max(value = 100, message = "Battery capacity should not be more than 100")
    @Min(value = 0, message = "Battery capacity should not be less than 0")
    private int batteryCapacity;

    @Enumerated(EnumType.STRING)
    @Column(name = "state", length = 50)
    private States state;

    @OneToMany(mappedBy = "drone")
    @JsonIgnore
    Set<Dispatch> dispatches;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Models getModel() {
        return model;
    }

    public void setModel(Models model) {
        this.model = model;
    }

    public int getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(int weightLimit) {
        this.weightLimit = weightLimit;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public States getState() {
        return state;
    }

    public void setState(States state) {
        this.state = state;
    }

    public Set<Dispatch> getDispatches() {
        return dispatches;
    }

    public Boolean canLoad() {
        return (state == States.IDLE || state == States.LOADING)
                && batteryCapacity >= 25;
    }

}