package com.ebiekuraju.oludotun.beans;

import java.util.ArrayList;
import java.util.List;

public class DispatchWrapper {
    private List<Integer> medicationIds;
    private String trackingCode;

    public DispatchWrapper() {
        medicationIds = new ArrayList<>();
    }

    public List<Integer> getMedicationIds() {
        return medicationIds;
    }

    public void setMedicationIds(List<Integer> medicationIds) {
        this.medicationIds = medicationIds;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }
}
