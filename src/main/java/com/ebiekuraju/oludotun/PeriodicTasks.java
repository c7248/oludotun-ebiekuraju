package com.ebiekuraju.oludotun;

import com.ebiekuraju.oludotun.beans.Drone;
import com.ebiekuraju.oludotun.repository.DroneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Iterator;

@Component
public class PeriodicTasks {

    @Autowired
    private DroneRepository droneRepository;

    private static final Logger log = LoggerFactory.getLogger(PeriodicTasks.class);

    @Scheduled(fixedRate = 5000)
    public void checkDroneBatteryLevel() {
        Iterator<Drone> droneIterator = droneRepository.findAll().iterator();
        while (droneIterator.hasNext()) {
            Drone drone = droneIterator.next();
            log.info("Serial #: {}, state: {}, battery level: {}",
                    drone.getSerialNumber(),
                    drone.getState(),
                    drone.getBatteryCapacity());
            //Set drone state to Idle if state is LOADING and battery level below 25%
            if(drone.getBatteryCapacity() < 25 && drone.getState() == Drone.States.LOADING) {
                log.info("Setting drone with serial #: {}, to IDLE state. Reason: battery less than 25%",
                        drone.getSerialNumber());
                drone.setState(Drone.States.IDLE);
                droneRepository.save(drone);
            }
        }
    }
}
